import 'package:flutter/material.dart';
import 'package:lab_7/login_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      themeMode: ThemeMode.system,
      title: "Login Page",
      home: LoginPage(),
    );
  }
}
