from django.urls import path
from . import views

urlpatterns = [
    path('', views.index_html),
    path('xml', views.index_xml),
    path('json', views.index_json),
]